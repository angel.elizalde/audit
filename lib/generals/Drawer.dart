import 'package:adulting/generals/Utils.dart';
import 'package:adulting/models/Usuario.dart';
import 'package:adulting/resources/Usuario.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';

class DrawerComponent extends StatelessWidget {
  const DrawerComponent({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    UsuarioProvider local = UsuarioProvider();
    final _size = MediaQuery.of(context).size;

    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          Container(
            height: _size.height * 0.3,
            child: DrawerHeader(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  FutureBuilder(
                    future: validarUsuario(),
                    builder: (BuildContext context,
                        AsyncSnapshot<UsuarioModel> snapshot) {
                      return Text(
                        snapshot.hasData ? snapshot.data.nombre : '',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: 18.0,
                        ),
                      );
                    },
                  ),
                  SizedBox(height: 8.0),
                  Text(
                    'Menu',
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
              decoration: BoxDecoration(
                  color: Colors.blue,
                  gradient: LinearGradient(
                      colors: [Color(0xFF3827B4), Color(0xFF6C18A4)])),
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Presupuestos'),
            onTap: () {
              Navigator.pushNamed(context, '/home');
            },
          ),
          ListTile(
            leading: Icon(Icons.category),
            title: Text('Clasificaciones'),
            onTap: () {
              Navigator.pushNamed(context, '/Clasificacion');
            },
          ),
          ListTile(
            leading: Icon(Icons.attach_money),
            title: Text('Ahorros'),
            onTap: () {
              Navigator.pushNamed(context, '/Ahorros');
            },
          ),
           ListTile(
            leading: Icon(Icons.favorite),
            title: Text('Salud Financiera'),
            onTap: () {
              Navigator.pushNamed(context, '/SaludFinanciera');
            },
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Cerrar sesión'),
            onTap: () {
              local.init().then((Database vlue) async {
                await local.delete();
              });
              Navigator.pushReplacementNamed(context, '/login');
            },
          ),
        ],
      ),
    );
  }
}
