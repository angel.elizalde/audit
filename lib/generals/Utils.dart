import 'package:adulting/models/Usuario.dart';
import 'package:adulting/resources/Usuario.dart';
import 'package:sqflite/sqflite.dart';

Future<UsuarioModel> validarUsuario() async {
  UsuarioModel resp;
  UsuarioProvider local = UsuarioProvider();
  await local.init().then((Database vlue) async {
    resp = await local.validate();
  });
  return resp;
}


  final meses = [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre'
  ];