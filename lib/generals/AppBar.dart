import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

Widget appBarComp = GradientAppBar(
  title: Row(
    children: <Widget>[
      Image(
        image: AssetImage('assets/img/iconx2.png'),
        height: 30.0,
        width: 30.0,
      ),
      SizedBox(width: 15.0),
      Text('Adulting'),
    ],
  ),
  gradient: LinearGradient(colors: [Color(0xFF3827B4), Color(0xFF6C18A4)]),
);
