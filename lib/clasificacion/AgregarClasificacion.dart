import 'package:adulting/generals/AppBar.dart';
import 'package:adulting/models/Clasificacion.dart';
import 'package:adulting/resources/Clasificacion.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';

class AgregarCategoria extends StatefulWidget {
  AgregarCategoria({Key key}) : super(key: key);

  @override
  _AgregarCategoriaState createState() => _AgregarCategoriaState();
}

class _AgregarCategoriaState extends State<AgregarCategoria> {
  ClasificacionesResource server = ClasificacionesResource();
  TextEditingController textController = new TextEditingController(text: '');

  @override
  Widget build(BuildContext context) {
    final nombreInpt = Container(
      margin: EdgeInsets.all(10.0),
      child: TextFormField(
        textInputAction: TextInputAction.next,
        textCapitalization: TextCapitalization.characters,
        controller: textController,
        onChanged: (String value) {
          if (textController.text != value.toUpperCase())
            textController.value =
                textController.value.copyWith(text: value.toUpperCase());
        },
        decoration: InputDecoration(
          labelText: 'Nombre',
          prefixIcon: Icon(Icons.title),
        ),
      ),
    );

    final buttonAceptar = GestureDetector(
      onTap: () async {
        // validaciones
        if (textController.text == '') {
          Flushbar(
            title: "Atención",
            message: "Falta capturar nombre de movimiento",
            duration: Duration(seconds: 2),
            backgroundColor: Colors.orange,
          )..show(context);
          return;
        }

        ClasificacionModel data = ClasificacionModel();
        data.nombre = textController.text.toUpperCase();
        FocusScope.of(context).requestFocus(FocusNode());
        final resp = await server.crearClasificacion(data);
        if (resp) {
          Navigator.pop(context);
        }
      },
      child: Container(
        margin: EdgeInsets.all(20.0),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(20.0),
        ),
        padding: EdgeInsets.all(10.0),
        child: Center(
          child: Text(
            'Agregar clasificacion',
            style: TextStyle(
              color: Colors.white,
              fontSize: 20.0,
            ),
          ),
        ),
      ),
    );

    return Scaffold(
      appBar: appBarComp,
      body: Container(
        child: Card(
          child: Column(
            children: <Widget>[
              nombreInpt,
              buttonAceptar,
            ],
          ),
        ),
      ),
    );
  }
}
