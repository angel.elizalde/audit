import 'package:adulting/clasificacion/AgregarClasificacion.dart';
import 'package:adulting/generals/AppBar.dart';
import 'package:adulting/generals/ClippWave.dart';
import 'package:adulting/generals/Drawer.dart';
import 'package:adulting/models/Clasificacion.dart';
import 'package:adulting/resources/Clasificacion.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class Clasificacion extends StatefulWidget {
  Clasificacion({Key key}) : super(key: key);

  @override
  _ClasificacionState createState() => _ClasificacionState();
}

class _ClasificacionState extends State<Clasificacion> {
  ClasificacionesResource server = ClasificacionesResource();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarComp,
      body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 140.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _backGround(),
                ],
              ),
            ),
            Expanded(
              child: FutureBuilder(
                future: server.getClasificacion(),
                builder: (BuildContext context,
                    AsyncSnapshot<List<ClasificacionModel>> snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Dismissible(
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(100.0),
                            ),
                            elevation: 5.0,
                            child: ListTile(
                              title: Text(snapshot.data[index].nombre),
                            ),
                          ),
                          key: UniqueKey(),
                          onDismissed: (direction) async {
                            final resp = await server
                                .deleteClasificacion(snapshot.data[index]);
                            if (!resp) {
                              Flushbar(
                                title: "Atención",
                                message:
                                    "Ya existe algun movimiento resgistrado con esta clasificación.",
                                duration: Duration(seconds: 2),
                                backgroundColor: Colors.orange,
                              )..show(context);
                              setState(() {});
                            }
                          },
                          confirmDismiss: (DismissDirection direction) async {
                            final bool res = await showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: const Text("Confirmar"),
                                  content: const Text(
                                      "Seguro que desea eliminar el registro?"),
                                  actions: <Widget>[
                                    FlatButton(
                                        onPressed: () =>
                                            Navigator.of(context).pop(true),
                                        child: const Text("ELIMINAR")),
                                    FlatButton(
                                      onPressed: () =>
                                          Navigator.of(context).pop(false),
                                      child: const Text("CANCELAR"),
                                    ),
                                  ],
                                );
                              },
                            );
                            return res;
                          },
                        );
                      },
                    );
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                },
              ),
            ),
          ]),
      drawer: DrawerComponent(),
    );
  }

  Widget _backGround() {
    return Stack(
      children: <Widget>[
        Transform(
            alignment: Alignment.center,
            transform: Matrix4.rotationY(math.pi),
            child: ClipPath(
              child: Container(
                height: 120,
                color: Theme.of(context).accentColor.withOpacity(0.7),
              ),
              clipper: BottomWaveClipper(),
            )),
        Transform(
            alignment: Alignment.center,
            transform: Matrix4.rotationY(math.pi),
            child: ClipPath(
              child: Container(
                height: 135,
                color: Theme.of(context).accentColor.withOpacity(0.3),
              ),
              clipper: BottomWaveClipper(),
            )),
        ClipPath(
          child: Container(
            height: 120,
            color: Theme.of(context).primaryColor.withOpacity(0.7),
            child: Stack(
              children: <Widget>[],
            ),
          ),
          clipper: BottomWaveClipper(),
        ),
        ClipPath(
          child: Container(
            height: 135,
            color: Theme.of(context).primaryColor.withOpacity(0.2),
            child: Stack(
              children: <Widget>[
                Center(
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Text(
                                'Clasificaciones',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 30.0,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: FloatingActionButton(
                                backgroundColor: Colors.white,
                                child: Icon(
                                  Icons.add,
                                  color: Theme.of(context).primaryColor,
                                ),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              AgregarCategoria()));
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          clipper: BottomWaveClipper(),
        ),
      ],
    );
  }
}
