import 'dart:convert';

import 'package:adulting/generals/Utils.dart';
import 'package:adulting/models/Movimiento.dart';
import 'package:adulting/models/Presupuesto.dart';
import 'package:http/http.dart' as http;

class MovimientosResource {
  final String _url = 'https://adulting-back.firebaseio.com/';

  Future<bool> crearMovimiento(MovimientoModel data) async {
    final url = '$_url/movimientos.json';
    final user = await validarUsuario();
    await http.post(url,
        body: json.encoder.convert({
          "idUsuario": user.id,
          "idPresupuesto": data.idPresupuesto,
          "nombre": data.nombre,
          "importe": data.importe,
          "clasificacion": data.clasificacion,
          "tipo": data.tipo,
          "ahorro": data.ahorro,
          "fecha": new DateTime.now().toString()
        }));
    return true;
  }

  Future<bool> deleteMovimiento(String id) async {
    final url = '$_url/movimientos/$id.json';
    await http.delete(url);
    return true;
  }

  Future<List<MovimientoModel>> getMovimientos(PresupuestoModel filtro) async {
    final url = '$_url/movimientos.json';
    final resp = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    List<MovimientoModel> listaMovimientos = new List();

    if (decodedData == null) {
      return [];
    }

    decodedData.forEach((id, data) {
      final temp = MovimientoModel.fromJson(data);
      temp.id = id;
      if (temp.idPresupuesto == filtro.id) {
        listaMovimientos.add(temp);
      }
    });

    return listaMovimientos;
  }

  Future<double> getTotalMovimientos(PresupuestoModel filtro) async {
    final url = '$_url/movimientos.json';
    final resp = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    List<MovimientoModel> listaMovimientos = new List();

    if (decodedData == null) {
      return 0.0;
    }
    double importe = double.parse(filtro.importe);

    decodedData.forEach((id, data) {
      final temp = MovimientoModel.fromJson(data);
      temp.id = id;
      if (temp.idPresupuesto == filtro.id) {
        listaMovimientos.add(temp);
        // Se calculan afecataciones de movimientos en presupuesto
        if (temp.tipo == 'ingreso') {
          importe = importe + double.parse(temp.importe);
        } else {
          importe = importe - double.parse(temp.importe);
        }
      }
    });

    return importe;
  }

  Future<List<MovimientoModel>> getAhorros() async {
    final url = '$_url/movimientos.json';
    final resp = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    List<MovimientoModel> listaMovimientos = new List();

    if (decodedData == null) {
      return [];
    }

    final user = await validarUsuario();

    if (user != null) {
      decodedData.forEach((id, data) {
        final temp = MovimientoModel.fromJson(data);
        temp.id = id;
        if (temp.idUsuario == user.id && temp.ahorro) {
          listaMovimientos.add(temp);
        }
      });
    }

    return listaMovimientos;
  }

  Future<double> getTotalAhorros() async {
    final url = '$_url/movimientos.json';
    final resp = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    List<MovimientoModel> listaMovimientos = new List();

    if (decodedData == null) {
      return 0.0;
    }
    double importe = 0.0;

    final user = await validarUsuario();
    if (user != null) {
      decodedData.forEach((id, data) {
        final temp = MovimientoModel.fromJson(data);
        temp.id = id;
        if (temp.idUsuario == user.id && temp.ahorro) {
          listaMovimientos.add(temp);
          // Se calculan afecataciones de movimientos en presupuesto
          importe = importe + double.parse(temp.importe);
        }
      });
    }
    return importe;
  }
}
