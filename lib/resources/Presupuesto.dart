import 'dart:convert';

import 'package:adulting/generals/Utils.dart';
import 'package:adulting/models/Movimiento.dart';
import 'package:adulting/models/Presupuesto.dart';
import 'package:adulting/resources/Movimientos.dart';
import 'package:http/http.dart' as http;

class PresupuestosResource {
  final String _url = 'https://adulting-back.firebaseio.com/';

  Future<bool> crearPresupuesto(PresupuestoModel data) async {
    final url = '$_url/presupuestos.json';
    final user = await validarUsuario();
    await http.post(url,
        body: json.encoder.convert({
          "idUsuario": user.id,
          "nombre": data.nombre,
          "importe": data.importe.toString(),
          "fecha": new DateTime.now().toString()
        }));
    return true;
  }

  Future<bool> deletePresupuesto(String id) async {
    MovimientosResource server = MovimientosResource();

    final url = '$_url/presupuestos/$id.json';
    await http.delete(url);
    // Se eliminan sus movimientos
    PresupuestoModel data = PresupuestoModel(id: id);
    final mov = await server.getMovimientos(data);
    mov.forEach((MovimientoModel item) async {
      await server.deleteMovimiento(item.id);
    });

    return true;
  }

  Future<List<PresupuestoModel>> getPresupuestos() async {
    final url = '$_url/presupuestos.json';
    final resp = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    List<PresupuestoModel> listaPresupuestos = new List();

    if (decodedData == null) {
      return [];
    }

    final user = await validarUsuario();

    decodedData.forEach((id, data) {
      final temp = PresupuestoModel.fromJson(data);
      temp.id = id;
      if (user != null && temp.idUsuario == user.id) {
        listaPresupuestos.add(temp);
      }
    });

    return listaPresupuestos;
  }

  Future<double> getTotalPresupuestos() async {
    final url = '$_url/presupuestos.json';
    final resp = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    double importe = 0.0;

    if (decodedData == null) {
      return 0.0;
    }

    final user = await validarUsuario();

    decodedData.forEach((id, data) {
      final temp = PresupuestoModel.fromJson(data);
      temp.id = id;
      if (user != null && temp.idUsuario == user.id) {
        importe = importe + double.parse(temp.importe);
      }
    });

    return importe;
  }
}
