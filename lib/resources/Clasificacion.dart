import 'dart:convert';

import 'package:adulting/generals/Utils.dart';
import 'package:adulting/models/Clasificacion.dart';
import 'package:adulting/models/Movimiento.dart';
import 'package:adulting/models/Presupuesto.dart';
import 'package:adulting/resources/Movimientos.dart';
import 'package:adulting/resources/Presupuesto.dart';
import 'package:http/http.dart' as http;

class ClasificacionesResource {
  final String _url = 'https://adulting-back.firebaseio.com/';

  Future<bool> crearClasificacion(ClasificacionModel data) async {
    final url = '$_url/clasificaciones.json';
    final user = await validarUsuario();

    await http.post(url,
        body: json.encoder.convert({
          "idUsuario": user.id,
          "nombre": data.nombre,
          "fecha": new DateTime.now().toString()
        }));
    return true;
  }

  Future<bool> deleteClasificacion(ClasificacionModel clas) async {
    final url = '$_url/clasificaciones/${clas.id}.json';

    // Se valida que ningun movimiento este en esa clasificación
    final encontro = await validarClasificacion(clas);
    if (!encontro) {
      await http.delete(url);
      return true;
    } else {
      return false;
    }
  }

  Future<bool> validarClasificacion(ClasificacionModel clas) async {
    bool encontro = false;
    MovimientosResource server = MovimientosResource();
    PresupuestosResource pres = PresupuestosResource();
    final presupuesto = await pres.getPresupuestos();
    for (PresupuestoModel data in presupuesto) {
      final mov = await server.getMovimientos(data);
      for (MovimientoModel item in mov) {
        if (item.clasificacion == clas.nombre) {
          encontro = true;
        }
      }
    }

    return encontro;
  }

  Future<List<ClasificacionModel>> getClasificacion() async {
    final url = '$_url/clasificaciones.json';
    final resp = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    List<ClasificacionModel> listaClasificacion = new List();

    if (decodedData == null) {
      return [];
    }

    final user = await validarUsuario();

    if (user != null) {
      decodedData.forEach((id, data) {
        final temp = ClasificacionModel.fromJson(data);
        temp.id = id;
        if (temp.idUsuario == user.id) {
          listaClasificacion.add(temp);
        }
      });
    }

    return listaClasificacion;
  }
}
