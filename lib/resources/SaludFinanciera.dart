import 'dart:convert';
import 'dart:math' as math;
import 'dart:math';
import 'dart:ui';
import 'package:adulting/generals/Utils.dart';
import 'package:adulting/models/Movimiento.dart';
import 'package:adulting/models/SaludFinanciera.dart';
import 'package:http/http.dart' as http;

class SaludFinancieraResource {
  String _url = 'https://adulting-back.firebaseio.com/';

  Future<List<double>> getDatos() async {
    final url = '$_url/movimientos.json';
    final resp = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    List<double> listaMovimientos = [];
    double ingresos = 0.0;
    double egresos = 0.0;

    if (decodedData == null) {
      return [];
    }

    final user = await validarUsuario();

    if (user != null) {
      decodedData.forEach((id, data) {
        final temp = MovimientoModel.fromJson(data);
        temp.id = id;
        if (temp.idUsuario == user.id) {
          if (temp.tipo == 'ingreso') {
            ingresos = ingresos + double.parse(temp.importe);
          } else {
            egresos = egresos + double.parse(temp.importe);
          }
        }
      });

      listaMovimientos.add(ingresos);
      listaMovimientos.add(egresos);
    }

    return listaMovimientos;
  }

  getColor() {
    final Random _random = math.Random();
    return Color.fromRGBO(
        _random.nextInt(256), _random.nextInt(256), _random.nextInt(256), 1.0);
  }

  Future<List<SaludFinancieraModel>> getDatosChart(String mes) async {
    final url = '$_url/movimientos.json';
    final resp = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    List<SaludFinancieraModel> listaMovimientos = [];
    if (decodedData == null) {
      return [];
    }

    final user = await validarUsuario();

    if (user != null) {
      decodedData.forEach((id, data) {
        final temp = MovimientoModel.fromJson(data);
        temp.id = id;
        final fecha = DateTime.parse(temp.fecha);
        if (temp.idUsuario == user.id && mes == meses[fecha.month - 1]) {
          // Se busca clasificacion en lista * Si esta se suma importe * si no se agrega clasificacion con importe
          final buscar = listaMovimientos
              .where((i) => i.clasificacion == temp.clasificacion)
              .toList();
          if (buscar.length > 0) {
            listaMovimientos.forEach((SaludFinancieraModel item) {
              if (item.clasificacion == temp.clasificacion) {
                item.importe = item.importe + double.parse(temp.importe);
              }
            });
          } else {
            listaMovimientos.add(new SaludFinancieraModel(
                clasificacion: temp.clasificacion,
                importe: double.parse(temp.importe),
                color: getColor()));
          }
        }
      });
    }

    return listaMovimientos;
  }
}
