import 'dart:convert';
import 'dart:io';

import 'package:adulting/models/Usuario.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';

final String tableTodo = 'Usuarios';
final String columnId = 'id';
final String columnTitle = 'usuario';
final String columnNombre = 'nombre';
final String columnEmail = 'email';
final String columnPass = 'password';

class UsuarioProvider {
  Database db;

  final String _url = 'https://adulting-back.firebaseio.com/';

  Future<Database> init() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'Users.db');

    db = await openDatabase(path, version: 3, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute('''
create table $tableTodo ( 
  $columnId text, 
  $columnTitle text not null,
  $columnNombre text not null,
  $columnEmail text not null,
  $columnPass text not null)
''');
    });
    return db;
  }

  Future<UsuarioModel> validate() async {
    List<Map> maps = await db.query(tableTodo);
    if (maps.length > 0) {
      return UsuarioModel.fromMap(maps[0]);
    }
    return null;
  }

  Future<bool> insert(UsuarioModel todo) async {
    await db.rawInsert(
        'INSERT INTO $tableTodo(id, usuario, nombre, email, password) VALUES(?, ?, ?, ?, ?)',
        [todo.id, todo.usuario, todo.nombre, todo.email, todo.password]);
    return true;
  }

  Future<UsuarioModel> getUsuario(int id) async {
    List<Map> maps = await db.query(tableTodo,
        columns: [columnId, columnPass, columnTitle],
        where: '$columnId = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return UsuarioModel.fromMap(maps.first);
    }
    return null;
  }

  Future<int> delete() async {
    return await db.delete(tableTodo);
  }

  Future<int> update(UsuarioModel todo) async {
    return await db.update(tableTodo, todo.toMap(),
        where: '$columnId = ?', whereArgs: [todo.id]);
  }

  Future close() async => db.close();

  Future<List<UsuarioModel>> iniciarSesion(UsuarioModel usuario) async {
    final url = '$_url/usuarios.json';
    final resp = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    List<UsuarioModel> listaPresupuestos = new List();

    if (decodedData == null) {
      return [];
    }

    decodedData.forEach((id, data) {
      final temp = UsuarioModel.fromJson(data);
      temp.id = id;
      if (temp.usuario == usuario.usuario &&
          temp.password == usuario.password) {
        listaPresupuestos.add(temp);
      }
    });

    return listaPresupuestos;
  }

  Future<List<UsuarioModel>> validarUsuarioRegistro(
      UsuarioModel usuario) async {
    final url = '$_url/usuarios.json';
    final resp = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    List<UsuarioModel> listaPresupuestos = new List();

    if (decodedData == null) {
      return [];
    }

    decodedData.forEach((id, data) {
      final temp = UsuarioModel.fromJson(data);
      temp.id = id;
      if (temp.usuario == usuario.usuario) {
        listaPresupuestos.add(temp);
      }
    });

    return listaPresupuestos;
  }

  Future<bool> crearUsuario(UsuarioModel data) async {
    final url = '$_url/usuarios.json';
    // Se valida si usuario existe ya
    final valid = await validarUsuarioRegistro(data);
    if (valid.length > 0) {
      return false;
    }

    await http.post(url,
        body: json.encoder.convert({
          "email": data.email,
          "password": data.password,
          "usuario": data.usuario,
          "nombre": data.nombre,
        }));
    return true;
  }
}
