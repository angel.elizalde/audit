import 'package:adulting/generals/AppBar.dart';
import 'package:adulting/generals/ClippWave.dart';
import 'package:adulting/generals/CurrencyFormat.dart';
import 'package:adulting/generals/Drawer.dart';
import 'package:adulting/models/Movimiento.dart';
import 'package:adulting/resources/Movimientos.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class Ahorro extends StatefulWidget {
  Ahorro({Key key}) : super(key: key);

  @override
  _AhorroState createState() => _AhorroState();
}

class _AhorroState extends State<Ahorro> {
  MovimientosResource server = MovimientosResource();
  @override
  Widget build(BuildContext context) {
    final _item = (MovimientoModel item) => Container(
          padding: EdgeInsets.all(8.0),
          margin: EdgeInsets.all(5.0),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(100.0),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 5.0,
                spreadRadius: 0.5,
                offset: Offset(0.0, 0.6),
              ),
            ],
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.monetization_on,
                        color: Colors.green,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 10.0),
                        child: Text(
                          item.nombre,
                          style: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Text(
                    r" $ " +
                        '${currencyString.format(double.parse(item.importe))}',
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ],
          ),
        );

    return Scaffold(
      appBar: appBarComp,
      body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 135,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _backGround(),
                ],
              ),
            ),
            Expanded(
              child: FutureBuilder(
                future: server.getAhorros(),
                builder: (BuildContext context,
                    AsyncSnapshot<List<MovimientoModel>> snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return _item(snapshot.data[index]);
                      },
                    );
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                },
              ),
            ),
          ]),
      drawer: DrawerComponent(),
    );
  }

  Widget _backGround() {
    return Stack(
      children: <Widget>[
        Transform(
            alignment: Alignment.center,
            transform: Matrix4.rotationY(math.pi),
            child: ClipPath(
              child: Container(
                height: 120,
                color: Theme.of(context).accentColor.withOpacity(0.7),
              ),
              clipper: BottomWaveClipper(),
            )),
        Transform(
            alignment: Alignment.center,
            transform: Matrix4.rotationY(math.pi),
            child: ClipPath(
              child: Container(
                height: 135,
                color: Theme.of(context).accentColor.withOpacity(0.3),
              ),
              clipper: BottomWaveClipper(),
            )),
        ClipPath(
          child: Container(
            height: 120,
            color: Theme.of(context).primaryColor.withOpacity(0.7),
            child: Stack(
              children: <Widget>[],
            ),
          ),
          clipper: BottomWaveClipper(),
        ),
        ClipPath(
          child: Container(
            height: 135,
            color: Theme.of(context).primaryColor.withOpacity(0.2),
            child: Stack(
              children: <Widget>[
                Center(
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Text(
                                'Ahorros',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 30.0,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                            FutureBuilder(
                              future: server.getTotalAhorros(),
                              builder: (BuildContext context,
                                  AsyncSnapshot<double> snapshot) {
                                return Container(
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        top: 8, bottom: 8, left: 20, right: 20),
                                    child: Text(
                                      snapshot.data != null
                                          ? r" $ " +
                                              '${currencyString.format(snapshot.data)}'
                                          : r" $ 0.00",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 20.0,
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          clipper: BottomWaveClipper(),
        ),
      ],
    );
  }
}
