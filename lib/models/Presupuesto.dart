import 'dart:convert';

String presupuestoToString(PresupuestoModel data) =>
    json.encode(data.toString());

class PresupuestoModel {
  String id;
  String idUsuario;
  String nombre;
  String importe;
  String fecha;
  

  PresupuestoModel({this.id, this.nombre, this.importe});

  PresupuestoModel.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    idUsuario = json["idUsuario"];
    nombre = json['nombre'];
    importe = json['importe'];
    fecha = json['fecha'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["id"] = this.id;
    data["idUsuario"] = this.idUsuario;
    data['nombre'] = this.nombre;
    data['importe'] = this.importe;
    data['fecha'] = this.fecha;
    return data;
  }
}
