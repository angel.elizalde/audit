import 'dart:ui';

class SaludFinancieraModel {
  String clasificacion;
  double importe;
  Color color;
  SaludFinancieraModel({this.clasificacion, this.importe, this.color});
}
