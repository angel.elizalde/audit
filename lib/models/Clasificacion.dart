class ClasificacionModel {
  String id;
  String idUsuario;
  String nombre;
  String fecha;

  ClasificacionModel({this.id, this.nombre});

  ClasificacionModel.fromJson(Map<String, dynamic> json) {
     id = json["id"];
    idUsuario = json["idUsuario"];
    nombre = json['nombre'];
    fecha = json['fecha'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data["idUsuario"] = this.idUsuario;
    data['nombre'] = this.nombre;
    data['fecha'] = this.fecha;
    return data;
  }
}