class MovimientoModel {
  String id;
  String idUsuario;
  String idPresupuesto;
  String nombre;
  String importe;
  String clasificacion;
  String tipo;
  bool ahorro;
  String fecha;

  MovimientoModel(
      {this.id,
      this.nombre,
      this.importe,
      this.clasificacion,
      this.tipo,
      this.ahorro});

  MovimientoModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idUsuario = json["idUsuario"];
    idPresupuesto = json['idPresupuesto'];
    nombre = json['nombre'];
    importe = json['importe'];
    clasificacion = json['clasificacion'];
    tipo = json['tipo'];
    ahorro = json['ahorro'];
    fecha = json['fecha'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data["idUsuario"] = this.idUsuario;
    data['idPresupuesto'] = this.idPresupuesto;
    data['nombre'] = this.nombre;
    data['importe'] = this.importe;
    data['clasificacion'] = this.clasificacion;
    data['tipo'] = this.tipo;
    data['ahorro'] = this.ahorro;
    data['fecha'] = this.fecha;
    return data;
  }
}
