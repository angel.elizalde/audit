class UsuarioModel {
  String id;
  String usuario;
  String nombre;
  String email;
  String password;

  UsuarioModel({this.usuario, this.password, this.nombre, this.email});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      usuario: usuario,
      email: email,
      nombre: nombre,
      password: password
    };
    if (id != null) {
      map["id"] = id;
    }
    return map;
  }

  UsuarioModel.fromMap(Map<String, dynamic> map) {
    id = map["id"];
    usuario = map["usuario"];
    nombre = map["nombre"];
    email = map["email"];
    password = map["password"];
  }

  UsuarioModel.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    usuario = json['usuario'];
    nombre = json["nombre"];
    email = json["email"];
    password = json['password'];
  }
}
