import 'package:adulting/models/Presupuesto.dart';
import 'package:adulting/models/Usuario.dart';
import 'package:flutter/material.dart';

class PresupuestoProvider with ChangeNotifier {
  String _importe;
  String _nombre;
  PresupuestoModel _presupuesto;
  String _total = '0.0';
  UsuarioModel _usuario;

  String get nombre => _nombre;

  set nombre(String value) {
    _nombre = value;
    notifyListeners();
  }

  String get importe => _importe;

  set importe(String value) {
    _importe = value;
    notifyListeners();
  }

  PresupuestoModel get presupuesto => _presupuesto;

  set presupuesto(PresupuestoModel value) {
    _presupuesto = value;
    notifyListeners();
  }

  String get total => _total;

  set total(String value) {
    _total = value;
    notifyListeners();
  }

  UsuarioModel get usuario => _usuario;

  set usuario(UsuarioModel value) {
    _usuario = value;
    notifyListeners();
  }
}
