import 'package:adulting/ahorros/Ahorros.dart';
import 'package:adulting/clasificacion/clasificacion.dart';
import 'package:adulting/home/home.dart';
import 'package:adulting/login/Login.dart';
import 'package:adulting/login/Registro.dart';
import 'package:adulting/presupuesto/presupuesto.dart';
import 'package:adulting/providers/Presupuestos.dart';
import 'package:adulting/salud-financiera/SaludFinanciera.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
            create: (BuildContext context) => PresupuestoProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primaryColor: Color(0xFF3827B4),
          accentColor: Color(0xFF6C18A4),
        ),
        initialRoute: '/home',
        routes: {
          '/login': (BuildContext context) => Login(),
          '/Registro': (BuildContext context) => Registrarse(),
          '/home': (BuildContext context) => MyHomePage(),
          '/Ahorros': (BuildContext context) => Ahorro(),
          '/Presupuesto': (BuildContext context) => Presupuesto(),
          '/Clasificacion': (BuildContext context) => Clasificacion(),
          '/SaludFinanciera': (BuildContext context) => SaludFinanciera(),
        },
      ),
    );
  }
}
