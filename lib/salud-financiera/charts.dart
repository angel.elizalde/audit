import 'package:adulting/generals/CurrencyFormat.dart';
import 'package:adulting/models/SaludFinanciera.dart';
import 'package:adulting/salud-financiera/Indicator.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class PieChartSample1 extends StatefulWidget {
  final List<SaludFinancieraModel> datos;
  PieChartSample1({this.datos});
  @override
  State<StatefulWidget> createState() =>
      PieChartSample1State(datos: this.datos);
}

class PieChartSample1State extends State {
  List<SaludFinancieraModel> datos;
  int touchedIndex;
  PieChartSample1State({this.datos});

  @override
  Widget build(BuildContext context) {
    return datos.length > 0
        ? AspectRatio(
            aspectRatio: 1.2,
            child: Card(
              elevation: 5.0,
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  const SizedBox(
                    height: 18,
                  ),
                  Expanded(
                    child: AspectRatio(
                      aspectRatio: 2,
                      child: PieChart(
                        PieChartData(
                            pieTouchData:
                                PieTouchData(touchCallback: (pieTouchResponse) {
                              setState(() {
                                if (pieTouchResponse.touchInput
                                        is FlLongPressEnd ||
                                    pieTouchResponse.touchInput is FlPanEnd) {
                                  touchedIndex = -1;
                                } else {
                                  touchedIndex =
                                      pieTouchResponse.touchedSectionIndex;
                                }
                              });
                            }),
                            startDegreeOffset: 180,
                            borderData: FlBorderData(
                              show: false,
                            ),
                            sectionsSpace: 0,
                            centerSpaceRadius: 0,
                            sections: showingSections(datos)),
                      ),
                    ),
                  ),
                  Wrap(
                    runSpacing: 5.0,
                    spacing: 5.0,
                    children: _titles(datos),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          )
        : Text('');
  }

  List<PieChartSectionData> showingSections(List<SaludFinancieraModel> datos) {
    return List.generate(
      datos.length,
      (i) {
        final isTouched = i == touchedIndex;
        final double opacity = isTouched ? 1 : 0.6;
        return PieChartSectionData(
          color: datos[i].color.withOpacity(opacity),
          value: datos[i].importe,
          title: r"$ " + '${currencyString.format(datos[i].importe)}',
          radius: 100,
          titleStyle: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.bold,
              color: const Color(0xff044d7c)),
          titlePositionPercentageOffset: 0.55,
        );
      },
    );
  }

  List<Widget> _titles(List<SaludFinancieraModel> datos) {
    return List.generate(datos.length, (i) {
      return Container(
        width: 150.0,
        child: Indicator(
          color: datos[i].color,
          text: datos[i].clasificacion,
          isSquare: false,
        ),
      );
    });
  }
}
