import 'package:adulting/generals/CurrencyFormat.dart';
import 'package:adulting/resources/Movimientos.dart';
import 'package:adulting/resources/Presupuesto.dart';
import 'package:adulting/resources/SaludFinanciera.dart';
import 'package:flutter/material.dart';

class HeaderInfo extends StatefulWidget {
  HeaderInfo({Key key}) : super(key: key);

  @override
  _HeaderInfoState createState() => _HeaderInfoState();
}

class _HeaderInfoState extends State<HeaderInfo> {
  SaludFinancieraResource server = SaludFinancieraResource();
  PresupuestosResource presupuesto = PresupuestosResource();
  MovimientosResource ahorro = MovimientosResource();

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5.0,
      child: Container(
        padding:
            EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            FutureBuilder(
              future: presupuesto.getTotalPresupuestos(),
              builder: (BuildContext context, AsyncSnapshot<double> snapshot) {
                if (snapshot.hasData) {
                  return Text(
                    r"Presupuestos: $ " +
                        '${currencyString.format(snapshot.data)}',
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.w700,
                    ),
                  );
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
            SizedBox(height: 20.0),
            FutureBuilder(
              future: server.getDatos(),
              builder:
                  (BuildContext context, AsyncSnapshot<List<double>> snapshot) {
                if (snapshot.hasData) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        snapshot.data.length > 0
                            ? r"Ingresos: $ " +
                                '${currencyString.format(snapshot.data[0])}'
                            : r"Ingresos: $ 0.0",
                        style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Text(
                        snapshot.data.length > 0
                            ? r"Egresos: $ " +
                                '${currencyString.format(snapshot.data[1])}'
                            : r"Egresos: $ 0.0",
                        style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  );
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
            SizedBox(height: 20.0),
            FutureBuilder(
              future: ahorro.getTotalAhorros(),
              builder: (BuildContext context, AsyncSnapshot<double> snapshot) {
                if (snapshot.hasData) {
                  return Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        r"Ahorros: $ " +
                            '${currencyString.format(snapshot.data)}',
                        style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  );
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
