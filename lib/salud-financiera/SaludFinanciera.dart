import 'package:adulting/generals/AppBar.dart';
import 'package:adulting/generals/ClippWave.dart';
import 'package:adulting/generals/Drawer.dart';
import 'package:adulting/generals/Utils.dart';
import 'package:adulting/models/SaludFinanciera.dart';
import 'package:adulting/resources/SaludFinanciera.dart';
import 'package:adulting/salud-financiera/charts.dart';
import 'package:adulting/salud-financiera/header.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class SaludFinanciera extends StatefulWidget {
  SaludFinanciera({Key key}) : super(key: key);

  @override
  _SaludFinancieraState createState() => _SaludFinancieraState();
}

class _SaludFinancieraState extends State<SaludFinanciera> {
  SaludFinancieraResource server = SaludFinancieraResource();
  String dropdownValue = '0';

  List<String> _meses = [];

  @override
  void initState() {
    final date = new DateTime.now().month;
    // Agregar meses a arreglo
    for (int mes = 1; mes <= date; mes++) {
      _meses.add(meses[mes - 1]);
    }

    // Seleccionar mes acutal
    dropdownValue = meses[date - 1];
    setState(() {});

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarComp,
      body: SingleChildScrollView(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: double.infinity,
                height: 135.0,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    _backGround(),
                  ],
                ),
              ),
              HeaderInfo(),
              FutureBuilder(
                future: server.getDatosChart(dropdownValue),
                builder: (BuildContext context,
                    AsyncSnapshot<List<SaludFinancieraModel>> snapshot) {
                  if (snapshot.hasData) {
                    return PieChartSample1(datos: snapshot.data);
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                },
              ),
            ]),
      ),
      drawer: DrawerComponent(),
    );
  }

  Widget _backGround() {
    return Stack(
      children: <Widget>[
        Transform(
            alignment: Alignment.center,
            transform: Matrix4.rotationY(math.pi),
            child: ClipPath(
              child: Container(
                height: 120,
                color: Theme.of(context).accentColor.withOpacity(0.7),
              ),
              clipper: BottomWaveClipper(),
            )),
        Transform(
            alignment: Alignment.center,
            transform: Matrix4.rotationY(math.pi),
            child: ClipPath(
              child: Container(
                height: 135,
                color: Theme.of(context).accentColor.withOpacity(0.3),
              ),
              clipper: BottomWaveClipper(),
            )),
        ClipPath(
          child: Container(
            height: 120,
            color: Theme.of(context).primaryColor.withOpacity(0.7),
            child: Stack(
              children: <Widget>[],
            ),
          ),
          clipper: BottomWaveClipper(),
        ),
        ClipPath(
          child: Container(
            height: 135,
            color: Theme.of(context).primaryColor.withOpacity(0.2),
            child: Stack(
              children: <Widget>[
                Center(
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Text(
                                'Salud Financiera',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 25.0,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                            Container(
                                margin: EdgeInsets.only(top: 10.0, right: 10.0),
                                padding: EdgeInsets.only(left: 10.0),
                                width: 150.0,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    color: Theme.of(context).accentColor,
                                    width: 2,
                                  ),
                                  borderRadius: BorderRadius.circular(100.0),
                                ),
                                child: DropdownButton<String>(
                                  value: dropdownValue,
                                  icon: Icon(
                                    Icons.arrow_downward,
                                    color: Theme.of(context).accentColor,
                                  ),
                                  iconSize: 24,
                                  elevation: 16,
                                  style: TextStyle(
                                      color: Theme.of(context).accentColor),
                                  underline: Container(
                                    height: 0,
                                  ),
                                  onChanged: (String newValue) {
                                    setState(() {
                                      dropdownValue = newValue;
                                    });
                                  },
                                  items: _meses.map<DropdownMenuItem<String>>(
                                      (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Container(
                                        child: Text(value),
                                        width: 100.0,
                                      ),
                                    );
                                  }).toList(),
                                ))
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          clipper: BottomWaveClipper(),
        ),
      ],
    );
  }
}
