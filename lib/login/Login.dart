import 'package:adulting/generals/AppBar.dart';
import 'package:adulting/models/Usuario.dart';
import 'package:adulting/resources/Usuario.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  UsuarioProvider server = UsuarioProvider();
  String _usuario = '';
  String _pass = '';
  bool _hide = true;

  @override
  Widget build(BuildContext context) {
    final nombreInpt = Container(
      margin: EdgeInsets.all(10.0),
      child: TextFormField(
        textInputAction: TextInputAction.next,
        onChanged: (String value) {
          _usuario = value;
        },
        decoration: InputDecoration(
          labelText: 'Usuario',
          prefixIcon: Icon(Icons.person),
        ),
      ),
    );

    final passInpt = Container(
      margin: EdgeInsets.all(10.0),
      child: TextFormField(
        textInputAction: TextInputAction.next,
        onChanged: (String value) {
          _pass = value;
        },
        obscureText: _hide,
        decoration: InputDecoration(
            labelText: 'Contraseña',
            prefixIcon: Icon(Icons.fingerprint),
            suffixIcon: IconButton(
              icon: _hide ? Icon(Icons.visibility) : Icon(Icons.visibility_off),
              onPressed: () {
                setState(() {
                  _hide = !_hide;
                });
              },
            )),
      ),
    );

    final buttonAceptar = GestureDetector(
      onTap: () async {
        // Validaciones
        if (_usuario == '') {
          Flushbar(
            title: "Atención",
            message: "Falta capturar usuario",
            duration: Duration(seconds: 2),
            backgroundColor: Colors.orange,
          )..show(context);
          return;
        }

        if (_pass == '' || _pass == '0.00') {
          Flushbar(
            title: "Atención",
            message: "Falta capturar contraseña",
            duration: Duration(seconds: 2),
            backgroundColor: Colors.orange,
          )..show(context);
          return;
        }

        UsuarioModel data = UsuarioModel(usuario: _usuario, password: _pass);

        FocusScope.of(context).requestFocus(FocusNode());
        final resp = await server.iniciarSesion(data);
        if (resp.length > 0) {
          // inicio sesion
          server.init().then((Database vlue) async {
            await server.insert(resp[0]);
            Navigator.pushReplacementNamed(context, '/home');
          });
        } else {
          // no inicio
          Flushbar(
            title: "Atención",
            message: "Usuario o contraseña incorrectos",
            duration: Duration(seconds: 2),
            backgroundColor: Colors.orange,
          )..show(context);
        }
      },
      child: Container(
        margin: EdgeInsets.all(20.0),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(20.0),
        ),
        padding: EdgeInsets.all(10.0),
        child: Center(
          child: Text(
            'iniciar sesión',
            style: TextStyle(
              color: Colors.white,
              fontSize: 20.0,
            ),
          ),
        ),
      ),
    );

    final _registrarse = Container(
      child: InkWell(
          child: Text(
            'No tengo cuenta, Registrarse!',
            style: TextStyle(
              fontSize: 15.0,
              color: Theme.of(context).primaryColor,
            ),
          ),
          onTap: () {
            Navigator.pushReplacementNamed(context, '/Registro');
          }),
    );

    return Scaffold(
      appBar: appBarComp,
      body: ListView(children: <Widget>[
        Card(
          margin: EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              nombreInpt,
              passInpt,
              buttonAceptar,
              _registrarse,
              SizedBox(height: 20.0),
            ],
          ),
        )
      ]),
    );
  }
}
