import 'package:adulting/generals/AppBar.dart';
import 'package:adulting/models/Usuario.dart';
import 'package:adulting/resources/Usuario.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';

class Registrarse extends StatefulWidget {
  Registrarse({Key key}) : super(key: key);

  @override
  _RegistrarseState createState() => _RegistrarseState();
}

class _RegistrarseState extends State<Registrarse> {
  UsuarioProvider server = UsuarioProvider();
  String _nombre = '';
  String _email = '';
  String _usuario = '';
  bool _hide = true;
  String _pass = '';

  @override
  Widget build(BuildContext context) {
    final usuarioInpt = Container(
      margin: EdgeInsets.all(10.0),
      child: TextFormField(
        textInputAction: TextInputAction.next,
        onChanged: (String value) {
          _usuario = value;
        },
        decoration: InputDecoration(
          labelText: 'Usuario',
          prefixIcon: Icon(Icons.person),
        ),
      ),
    );

    final nombreInpt = Container(
      margin: EdgeInsets.all(10.0),
      child: TextFormField(
        textInputAction: TextInputAction.next,
        onChanged: (String value) {
          _nombre = value;
        },
        decoration: InputDecoration(
          labelText: 'Nombre',
          prefixIcon: Icon(Icons.description),
        ),
      ),
    );

    final passInpt = Container(
      margin: EdgeInsets.all(10.0),
      child: TextFormField(
        textInputAction: TextInputAction.next,
        onChanged: (String value) {
          _pass = value;
        },
        obscureText: _hide,
        decoration: InputDecoration(
            labelText: 'Contraseña',
            prefixIcon: Icon(Icons.fingerprint),
            suffixIcon: IconButton(
              icon: _hide ? Icon(Icons.visibility) : Icon(Icons.visibility_off),
              onPressed: () {
                setState(() {
                  _hide = !_hide;
                });
              },
            )),
      ),
    );

    final emailInpt = Container(
      margin: EdgeInsets.all(10.0),
      child: TextFormField(
        textInputAction: TextInputAction.next,
        keyboardType: TextInputType.emailAddress,
        onChanged: (String value) {
          _email = value;
        },
        decoration: InputDecoration(
          labelText: 'Email',
          prefixIcon: Icon(Icons.mail),
        ),
      ),
    );

    final buttonAceptar = GestureDetector(
      onTap: () async {
        // Validaciones
        if (_usuario == '') {
          Flushbar(
            title: "Atención",
            message: "Falta capturar usuario",
            duration: Duration(seconds: 2),
            backgroundColor: Colors.orange,
          )..show(context);
          return;
        }
        if (_nombre == '') {
          Flushbar(
            title: "Atención",
            message: "Falta capturar nombre",
            duration: Duration(seconds: 2),
            backgroundColor: Colors.orange,
          )..show(context);
          return;
        }

        if (_email == '') {
          Flushbar(
            title: "Atención",
            message: "Falta capturar email",
            duration: Duration(seconds: 2),
            backgroundColor: Colors.orange,
          )..show(context);
          return;
        }

        if (_pass == '' || _pass == '0.00') {
          Flushbar(
            title: "Atención",
            message: "Falta capturar contraseña",
            duration: Duration(seconds: 2),
            backgroundColor: Colors.orange,
          )..show(context);
          return;
        }

        UsuarioModel data = UsuarioModel(
          usuario: _usuario,
          password: _pass,
          nombre: _nombre,
          email: _email,
        );

        FocusScope.of(context).requestFocus(FocusNode());
        final resp = await server.crearUsuario(data);
        if (resp) {
          // inicio sesion
          final user = await server.iniciarSesion(data);
          server.init().then((Database vlue) async {
            await server.insert(user[0]);
            Navigator.pushReplacementNamed(context, '/home');
          });
        } else {
          // no inicio
          Flushbar(
            title: "Atención",
            message: "Usuario ya registrado, intente con otro.",
            duration: Duration(seconds: 2),
            backgroundColor: Colors.orange,
          )..show(context);
        }
      },
      child: Container(
        margin: EdgeInsets.all(20.0),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(20.0),
        ),
        padding: EdgeInsets.all(10.0),
        child: Center(
          child: Text(
            'Registrarse',
            style: TextStyle(
              color: Colors.white,
              fontSize: 20.0,
            ),
          ),
        ),
      ),
    );

    final _sesion = Container(
      child: InkWell(
          child: Text(
            'tengo cuenta, Iniciar sesión!',
            style: TextStyle(
              fontSize: 15.0,
              color: Theme.of(context).primaryColor,
            ),
          ),
          onTap: () {
            Navigator.pushReplacementNamed(context, '/login');
          }),
    );

    return Scaffold(
      appBar: appBarComp,
      body: ListView(
        children: <Widget>[
          Card(
            margin: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                usuarioInpt,
                nombreInpt,
                emailInpt,
                passInpt,
                buttonAceptar,
                _sesion,
                SizedBox(height: 20.0),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
