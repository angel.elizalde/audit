import 'package:adulting/generals/AppBar.dart';
import 'package:adulting/generals/ClippWave.dart';
import 'package:adulting/generals/CurrencyFormat.dart';
import 'package:adulting/models/Movimiento.dart';
import 'package:adulting/presupuesto/AgregarMovimiento.dart';
import 'package:adulting/providers/Presupuestos.dart';
import 'package:adulting/resources/Movimientos.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

import 'package:provider/provider.dart';

class Presupuesto extends StatefulWidget {
  Presupuesto({Key key}) : super(key: key);

  @override
  _PresupuestoState createState() => _PresupuestoState();
}

class _PresupuestoState extends State<Presupuesto> {
  MovimientosResource server = MovimientosResource();

  @override
  Widget build(BuildContext context) {
    final datos = Provider.of<PresupuestoProvider>(context);
    final cwidth = MediaQuery.of(context).size.width * 0.5;

    final _item = (MovimientoModel item) => Dismissible(
          child: Container(
            padding: EdgeInsets.only(
              top: 8.0,
              bottom: 8.0,
              left: 15.0,
              right: 15.0,
            ),
            margin: EdgeInsets.all(5.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100.0),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  blurRadius: 5.0,
                  spreadRadius: 0.5,
                  offset: Offset(0.0, 0.6),
                ),
              ],
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.monetization_on,
                          color: item.tipo == 'ingreso'
                              ? Colors.green
                              : Colors.red,
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10.0),
                          child: Container(
                            width: cwidth,
                            child: Text(
                              item.nombre,
                              style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Text(
                      r" $ " +
                          '${currencyString.format(double.parse(item.importe))}',
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        item.clasificacion,
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 12,
                        ),
                      ),
                      Text(
                        item.ahorro ? 'AHORRO' : '',
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 12,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          key: UniqueKey(),
          onDismissed: (direction) {
            server.deleteMovimiento(item.id);
            setState(() {});
          },
          confirmDismiss: (DismissDirection direction) async {
            final bool res = await showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: const Text("Confirmar"),
                  content: const Text("Seguro que desea eliminar el registro?"),
                  actions: <Widget>[
                    FlatButton(
                        onPressed: () => Navigator.of(context).pop(true),
                        child: const Text("ELIMINAR")),
                    FlatButton(
                      onPressed: () => Navigator.of(context).pop(false),
                      child: const Text("CANCELAR"),
                    ),
                  ],
                );
              },
            );
            return res;
          },
        );

    Widget _backGround() {
      return Stack(
        children: <Widget>[
          Transform(
              alignment: Alignment.center,
              transform: Matrix4.rotationY(math.pi),
              child: ClipPath(
                child: Container(
                  height: 180,
                  color: Theme.of(context).accentColor.withOpacity(0.5),
                ),
                clipper: BottomWaveClipper(),
              )),
          Transform(
              alignment: Alignment.center,
              transform: Matrix4.rotationY(math.pi),
              child: ClipPath(
                child: Container(
                  height: 190,
                  color: Theme.of(context).accentColor.withOpacity(0.5),
                ),
                clipper: BottomWaveClipper(),
              )),
          Transform(
              alignment: Alignment.center,
              transform: Matrix4.rotationY(math.pi),
              child: ClipPath(
                child: Container(
                  height: 200,
                  color: Theme.of(context).accentColor.withOpacity(0.3),
                ),
                clipper: BottomWaveClipper(),
              )),
          ClipPath(
            child: Container(
              height: 180,
              color: Theme.of(context).primaryColor.withOpacity(0.5),
              child: Stack(
                children: <Widget>[],
              ),
            ),
            clipper: BottomWaveClipper(),
          ),
          ClipPath(
            child: Container(
              height: 190,
              color: Theme.of(context).primaryColor.withOpacity(0.3),
              child: Stack(
                children: <Widget>[],
              ),
            ),
            clipper: BottomWaveClipper(),
          ),
          ClipPath(
            child: Container(
              height: 200,
              color: Theme.of(context).primaryColor.withOpacity(0.2),
              child: Stack(
                children: <Widget>[
                  Center(
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 10.0),
                          child: Text(
                            'Total ' + datos.presupuesto.nombre,
                            style:
                                TextStyle(color: Colors.white, fontSize: 17.0),
                          ),
                        ),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Text(
                                r" $ " +
                                    '${currencyString.format(double.parse(datos.presupuesto.importe))}',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 25.0,
                                    fontWeight: FontWeight.w500),
                              ),
                              SizedBox(width: 40.0),
                              FloatingActionButton(
                                backgroundColor: Colors.white,
                                child: Icon(
                                  Icons.add,
                                  color: Theme.of(context).primaryColor,
                                ),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              AgregarMovimiento()));
                                },
                              ),
                              SizedBox(width: 40.0),
                            ],
                          ),
                        ),
                        FutureBuilder(
                          future: server.getTotalMovimientos(datos.presupuesto),
                          builder: (BuildContext context,
                              AsyncSnapshot<double> snapshot) {
                            if (snapshot.hasData) {
                              return Container(
                                decoration: BoxDecoration(
                                  color: Colors.white.withOpacity(0.8),
                                  borderRadius: BorderRadius.circular(5.0),
                                ),
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      top: 5, bottom: 5, left: 20, right: 20),
                                  child: Text(
                                    snapshot.data != null
                                        ? r" $ " +
                                            '${currencyString.format(snapshot.data)}'
                                        : r" $ 0.00",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: snapshot.data < 0
                                          ? Colors.red
                                          : Colors.green,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                ),
                              );
                            } else {
                              return Text('');
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            clipper: BottomWaveClipper(),
          ),
        ],
      );
    }

    return Scaffold(
      appBar: appBarComp,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: double.infinity,
            height: 235.0,
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  blurRadius: 5.0,
                  spreadRadius: 0.5,
                  offset: Offset(0.0, 0.6),
                ),
              ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _backGround(),
                Container(
                  margin: EdgeInsets.only(left: 20.0),
                  child: Text(
                    'Movimientos',
                    style:
                        TextStyle(fontSize: 18.0, fontWeight: FontWeight.w600),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: FutureBuilder(
              future: server.getMovimientos(datos.presupuesto),
              builder: (BuildContext context,
                  AsyncSnapshot<List<MovimientoModel>> snapshot) {
                if (snapshot.hasData) {
                  return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      return _item(snapshot.data[index]);
                    },
                  );
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
