import 'package:adulting/generals/AppBar.dart';
import 'package:adulting/generals/ClippWave.dart';
import 'package:adulting/generals/CurrencyFormat.dart';
import 'package:adulting/models/Clasificacion.dart';
import 'package:adulting/models/Movimiento.dart';
import 'package:adulting/providers/Presupuestos.dart';
import 'package:adulting/resources/Clasificacion.dart';
import 'package:adulting/resources/Movimientos.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class AgregarMovimiento extends StatefulWidget {
  AgregarMovimiento({Key key}) : super(key: key);

  @override
  _AgregarMovimientoState createState() => _AgregarMovimientoState();
}

enum SingingCharacter { ingreso, egreso }

class _AgregarMovimientoState extends State<AgregarMovimiento> {
  SingingCharacter _character = SingingCharacter.ingreso;
  ClasificacionesResource server = ClasificacionesResource();
  MovimientosResource post = MovimientosResource();
  String _clasificacion = '';
  String _nombre = '';
  String _importe = '';
  bool _ahorro = false;

  @override
  Widget build(BuildContext context) {
    final presupuesto = Provider.of<PresupuestoProvider>(context);

    final nombreInpt = Container(
      margin: EdgeInsets.only(left: 10.0, right: 10.0),
      child: TextFormField(
        textInputAction: TextInputAction.next,
        maxLength: 50,
        onChanged: (String value) {
          setState(() {
            _nombre = value;
          });
        },
        decoration: InputDecoration(
          labelText: 'Nombre',
          prefixIcon: Icon(Icons.title),
        ),
      ),
    );

    final importeInpt = Container(
      margin: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
      child: TextFormField(
        keyboardType: TextInputType.number,
        onChanged: (String value) {
          setState(() {
            _importe = value;
          });
        },
        inputFormatters: [
          WhitelistingTextInputFormatter.digitsOnly,
          CurrencyInputFormatter()
        ],
        decoration: InputDecoration(
          labelText: 'Importe',
          prefixIcon: Icon(Icons.attach_money),
        ),
      ),
    );

    final descripcionInpt = Container(
      margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0, bottom: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Text('Clasificación'),
          ),
          FutureBuilder(
            future: server.getClasificacion(),
            builder: (BuildContext context,
                AsyncSnapshot<List<ClasificacionModel>> snapshot) {
              if (snapshot.hasData) {
                return Wrap(
                  alignment: WrapAlignment.center,
                  spacing: 12.0,
                  children: List<Widget>.generate(
                    snapshot.data.length,
                    (int index) {
                      return ChoiceChip(
                        pressElevation: 0.0,
                        selectedColor: Colors.blue,
                        backgroundColor: Colors.grey[100],
                        label: Text(snapshot.data[index].nombre),
                        selected: _clasificacion == snapshot.data[index].nombre,
                        onSelected: (bool selected) {
                          setState(() {
                            _clasificacion =
                                selected ? snapshot.data[index].nombre : '';
                          });
                        },
                      );
                    },
                  ).toList(),
                );
              } else {
                return Center(child: CircularProgressIndicator());
              }
            },
          ),
        ],
      ),
    );

    final buttonAceptar = GestureDetector(
      onTap: () async {
        // validaciones
        if (_nombre == '') {
          Flushbar(
            title: "Atención",
            message: "Falta capturar nombre de movimiento",
            duration: Duration(seconds: 2),
            backgroundColor: Colors.orange,
          )..show(context);
          return;
        }

        if (_importe == '' || _importe == '0.00') {
          Flushbar(
            title: "Atención",
            message: "Falta capturar importe de movimiento",
            duration: Duration(seconds: 2),
            backgroundColor: Colors.orange,
          )..show(context);
          return;
        }

        if (_clasificacion == '') {
          Flushbar(
            title: "Atención",
            message: "Falta capturar una calsificación para movimiento",
            duration: Duration(seconds: 2),
            backgroundColor: Colors.orange,
          )..show(context);
          return;
        }

        MovimientoModel datos = new MovimientoModel();
        datos.idPresupuesto = presupuesto.presupuesto.id;
        datos.nombre = _nombre;
        datos.importe = _importe.replaceAll(',', '');
        datos.clasificacion = _clasificacion;
        datos.tipo =
            _character == SingingCharacter.egreso ? 'egreso' : 'ingreso';
        datos.ahorro = _ahorro;

        FocusScope.of(context).requestFocus(FocusNode());
        final resp = await post.crearMovimiento(datos);
        if (resp) {
          Navigator.pop(context);
        }
      },
      child: Container(
        margin: EdgeInsets.all(20.0),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(20.0),
        ),
        padding: EdgeInsets.all(10.0),
        child: Center(
          child: Text(
            'Agregar movimiento',
            style: TextStyle(
              color: Colors.white,
              fontSize: 20.0,
            ),
          ),
        ),
      ),
    );

    return Scaffold(
      appBar: appBarComp,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 60.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _backGround(),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10, bottom: 10),
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 5.0,
                    spreadRadius: 0.5,
                    offset: Offset(0.0, 0.6),
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10.0),
                    child: Text(
                      'Detalles de movimiento',
                      style: TextStyle(
                        fontSize: 18.0,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  nombreInpt,
                  importeInpt,
                  descripcionInpt,
                  CheckboxListTile(
                    title: Text("Agregar a ahorro"),
                    onChanged: (bool value) {
                      setState(() {
                        _ahorro = value;
                      });
                    },
                    value: _ahorro,
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10, bottom: 10),
              width: double.infinity,
              height: 160.0,
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 5.0,
                    spreadRadius: 0.5,
                    offset: Offset(0.0, 0.6),
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10.0),
                    child: Text(
                      'Seleccione tipo de movimiento',
                      style: TextStyle(
                        fontSize: 18.0,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  RadioListTile<SingingCharacter>(
                    title: const Text('Ingreso'),
                    value: SingingCharacter.ingreso,
                    groupValue: _character,
                    onChanged: (SingingCharacter value) {
                      setState(() {
                        _character = value;
                      });
                    },
                  ),
                  RadioListTile<SingingCharacter>(
                    title: const Text('Egreso'),
                    value: SingingCharacter.egreso,
                    groupValue: _character,
                    onChanged: (SingingCharacter value) {
                      setState(() {
                        _character = value;
                      });
                    },
                  ),
                ],
              ),
            ),
            buttonAceptar,
          ],
        ),
      ),
    );
  }

  Widget _backGround() {
    return Stack(
      children: <Widget>[
        Transform(
            alignment: Alignment.center,
            transform: Matrix4.rotationY(math.pi),
            child: ClipPath(
              child: Container(
                height: 40,
                color: Theme.of(context).accentColor.withOpacity(0.7),
              ),
              clipper: BottomWaveClipper(),
            )),
        Transform(
            alignment: Alignment.center,
            transform: Matrix4.rotationY(math.pi),
            child: ClipPath(
              child: Container(
                height: 60,
                color: Theme.of(context).accentColor.withOpacity(0.3),
              ),
              clipper: BottomWaveClipper(),
            )),
        ClipPath(
          child: Container(
            height: 40,
            color: Theme.of(context).primaryColor.withOpacity(0.7),
            child: Stack(
              children: <Widget>[],
            ),
          ),
          clipper: BottomWaveClipper(),
        ),
        ClipPath(
          child: Container(
            height: 60,
            color: Theme.of(context).primaryColor.withOpacity(0.2),
            child: Stack(
              children: <Widget>[],
            ),
          ),
          clipper: BottomWaveClipper(),
        ),
      ],
    );
  }
}
