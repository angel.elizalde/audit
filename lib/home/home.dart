import 'package:adulting/generals/AppBar.dart';
import 'package:adulting/generals/ClippWave.dart';
import 'package:adulting/generals/CurrencyFormat.dart';
import 'package:adulting/generals/Drawer.dart';
import 'package:adulting/home/AgregarPresupuesto.dart';
import 'package:adulting/models/Presupuesto.dart';
import 'package:adulting/providers/Presupuestos.dart';
import 'package:adulting/resources/Presupuesto.dart';
import 'package:adulting/resources/Usuario.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

import 'package:provider/provider.dart';
import 'package:sqflite/sqflite.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  PresupuestosResource server = PresupuestosResource();
  UsuarioProvider local = UsuarioProvider();

  @override
  void initState() {
    // Validar sqflite
    local.init().then((Database vlue) async {
      final resp = await local.validate();
      if (resp == null) {
        Navigator.pushReplacementNamed(context, '/login');
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final datos = Provider.of<PresupuestoProvider>(context);
    final cwidth = MediaQuery.of(context).size.width * 0.8;

    final _item = (PresupuestoModel item) => GestureDetector(
          onTap: () {
            datos.presupuesto = item;
            Navigator.pushNamed(context, '/Presupuesto');
          },
          child: Dismissible(
            child: Container(
              padding: EdgeInsets.only(
                top: 8.0,
                bottom: 8.0,
                left: 15.0,
                right: 15.0,
              ),
              margin: EdgeInsets.all(5.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(100.0),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 5.0,
                    spreadRadius: 0.5,
                    offset: Offset(0.0, 0.6),
                  ),
                ],
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: cwidth,
                        child: Text(
                          item.nombre,
                          style: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10.0),
                    child: Text(
                      r" $ " +
                          '${currencyString.format(double.parse(item.importe))}',
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontSize: 12,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            key: UniqueKey(),
            onDismissed: (direction) {
              server.deletePresupuesto(item.id);
            },
            confirmDismiss: (DismissDirection direction) async {
              final bool res = await showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: const Text("Confirmar"),
                    content:
                        const Text("Seguro que desea eliminar el registro?"),
                    actions: <Widget>[
                      FlatButton(
                          onPressed: () => Navigator.of(context).pop(true),
                          child: const Text("ELIMINAR")),
                      FlatButton(
                        onPressed: () => Navigator.of(context).pop(false),
                        child: const Text("CANCELAR"),
                      ),
                    ],
                  );
                },
              );
              return res;
            },
          ),
        );

    return Scaffold(
      appBar: appBarComp,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: double.infinity,
            height: 135.0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _backGround(),
              ],
            ),
          ),
          Expanded(
            child: FutureBuilder(
              future: server.getPresupuestos(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<PresupuestoModel>> snapshot) {
                if (snapshot.hasData) {
                  return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      return _item(snapshot.data[index]);
                    },
                  );
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
          ),
        ],
      ),
      drawer: DrawerComponent(),
    );
  }

  Widget _backGround() {
    return Stack(
      children: <Widget>[
        Transform(
            alignment: Alignment.center,
            transform: Matrix4.rotationY(math.pi),
            child: ClipPath(
              child: Container(
                height: 120,
                color: Theme.of(context).accentColor.withOpacity(0.7),
              ),
              clipper: BottomWaveClipper(),
            )),
        Transform(
            alignment: Alignment.center,
            transform: Matrix4.rotationY(math.pi),
            child: ClipPath(
              child: Container(
                height: 135,
                color: Theme.of(context).accentColor.withOpacity(0.3),
              ),
              clipper: BottomWaveClipper(),
            )),
        ClipPath(
          child: Container(
            height: 120,
            color: Theme.of(context).primaryColor.withOpacity(0.7),
            child: Stack(
              children: <Widget>[],
            ),
          ),
          clipper: BottomWaveClipper(),
        ),
        ClipPath(
          child: Container(
            height: 135,
            color: Theme.of(context).primaryColor.withOpacity(0.2),
            child: Stack(
              children: <Widget>[
                Center(
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Text(
                                'Presupuestos',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 30.0,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: FloatingActionButton(
                                backgroundColor: Colors.white,
                                child: Icon(
                                  Icons.add,
                                  color: Theme.of(context).primaryColor,
                                ),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              AgregarPresupueto()));
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          clipper: BottomWaveClipper(),
        ),
      ],
    );
  }
}
