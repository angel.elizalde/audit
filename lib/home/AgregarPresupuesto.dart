import 'package:adulting/generals/AppBar.dart';
import 'package:adulting/generals/CurrencyFormat.dart';
import 'package:adulting/models/Presupuesto.dart';
import 'package:adulting/resources/Presupuesto.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AgregarPresupueto extends StatefulWidget {
  AgregarPresupueto({Key key}) : super(key: key);

  @override
  _AgregarPresupuetoState createState() => _AgregarPresupuetoState();
}

class _AgregarPresupuetoState extends State<AgregarPresupueto> {
  @override
  Widget build(BuildContext context) {
    PresupuestosResource server = PresupuestosResource();
    String _nombre = '';
    String _importe = '0.00';

    final nombreInpt = Container(
      margin: EdgeInsets.only(left: 10.0, right: 10.0),
      child: TextFormField(
        textInputAction: TextInputAction.next,
        maxLength: 50,
        onChanged: (String value) {
          _nombre = value;
        },
        decoration: InputDecoration(
          labelText: 'Nombre',
          prefixIcon: Icon(Icons.description),
        ),
      ),
    );

    final importeInpt = Container(
      margin: EdgeInsets.only(left: 10.0, right: 10.0),
      child: TextField(
        onChanged: (String value) {
          _importe = value;
        },
        keyboardType: TextInputType.number,
        inputFormatters: [
          WhitelistingTextInputFormatter.digitsOnly,
          CurrencyInputFormatter()
        ],
        decoration: InputDecoration(
          labelText: 'Importe',
          prefixIcon: Icon(Icons.attach_money),
        ),
      ),
    );

    final buttonAceptar = GestureDetector(
      onTap: () async {
        // Validaciones
        if (_nombre == '') {
          Flushbar(
            title: "Atención",
            message: "Falta capturar nombre de presupuesto",
            duration: Duration(seconds: 2),
            backgroundColor: Colors.orange,
          )..show(context);
          return;
        }

        if (_importe == '' || _importe == '0.00') {
          Flushbar(
            title: "Atención",
            message: "Falta capturar importe para presupuesto",
            duration: Duration(seconds: 2),
            backgroundColor: Colors.orange,
          )..show(context);
          return;
        }

        PresupuestoModel data = PresupuestoModel();
        data.nombre = _nombre;
        data.importe = _importe.replaceAll(',', '');
        FocusScope.of(context).requestFocus(FocusNode());

        final resp = await server.crearPresupuesto(data);
        if (resp) {
          Navigator.pop(context);
        }
      },
      child: Container(
        margin: EdgeInsets.all(20.0),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(20.0),
        ),
        padding: EdgeInsets.all(10.0),
        child: Center(
          child: Text(
            'Agregar presupuesto',
            style: TextStyle(
              color: Colors.white,
              fontSize: 20.0,
            ),
          ),
        ),
      ),
    );

    return Scaffold(
      appBar: appBarComp,
      body: Container(
        padding: EdgeInsets.all(20.0),
        margin: EdgeInsets.all(10.0),
        height: 290.0,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 5.0,
              spreadRadius: 0.5,
              offset: Offset(0.0, 0.6),
            ),
          ],
        ),
        child: Column(
          children: <Widget>[
            nombreInpt,
            importeInpt,
            buttonAceptar,
          ],
        ),
      ),
    );
  }
}
